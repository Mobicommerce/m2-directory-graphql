<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_DirectoryGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\DirectoryGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Currency implements ResolverInterface
{
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\CurrencySymbol\Model\System\Currencysymbol $currencysymbol
    ) {
        $this->storeManager = $storeManager;
        $this->currencysymbol = $currencysymbol;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $store = $this->storeManager->getStore();

        $currencyCodes = $store->getAvailableCurrencyCodes(true);
        $currencyData = $this->currencysymbol->getCurrencySymbolsData();

        $result = [];
        foreach ($currencyCodes as $code) {
            $result[] = ['code' => $code, 'symbol' => $currencyData[$code]['displaySymbol']];
        }

        return $result;
    }
}
